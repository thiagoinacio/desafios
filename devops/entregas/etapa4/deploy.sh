#!/bin/bash

echo "Iniciando implantação"

kubectl get all -n challenge

kubectl apply -f backend/*.yml
kubectl apply -f frontend/*.yml
kubectl apply -f mondodb/*.yml

echo "Implantação finalizada"

kubectl get all -n challenge
